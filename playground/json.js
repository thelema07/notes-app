
/* let personString = '{"name":"Daniel","age":33}';
let person = JSON.parse(personString);
console.log(typeof person);
console.log(person); */

const fs = require('fs');

let originalNote = {
    title: 'Some title',
    body: 'Some body'
};

let originalNoteString = JSON.stringify(originalNote);
console.log(originalNoteString);
fs.writeFileSync('notes.json',originalNoteString);

let noteString = fs.readFileSync('notes.json');

let note = JSON.parse(originalNoteString);
console.log(note.title);
console.log(typeof note);