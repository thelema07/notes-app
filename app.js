console.log('Starting app...');

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes');

const argv = yargs.argv;
let command = argv._[0];
console.log("Command: ",command);

console.log('Process',process.argv);
console.log('Yargs',argv);

if(command === 'add'){
    let note = notes.addNote(argv.title,argv.body);

    if(note == undefined){
        console.log('Note already exists',note);
    } else {
        console.log('Note Created',note);
    }
} else if(command === 'list'){
    notes.getAll();
} else if(command === 'read'){
    let noteRead = notes.readNote(argv.title);
    if(noteRead){
        console.log('Note Found !!');
    } else {
        console.log('Note not found :(');
    }
} else if (command === 'delete'){
    let noteRemoved = notes.deleteNote(argv.title);
    let message = noteRemoved ? 'Note was removed' : 'Note not exists';
    console.log(message); 
}else {
    console.log('Command not recognized');
}

